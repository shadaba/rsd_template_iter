from __future__ import print_function
#To import the the functions for sampling from my directory
import sys
import os

import numpy as np
import pylab as pl

import matplotlib
matplotlib.rcParams.update({'font.size': 22, 'font.family': 'serif'})

import mycosmo as mcosm

from scipy.optimize import minimize

import argparse

if __name__=="__main__":
   parser = argparse.ArgumentParser(description='Find the Best HOD parameters:')
   #these should be input
   parser.add_argument('-iter_file',default='test_iter.txt')
   parser.add_argument('-Obh2',type=float,default=0.0226)
   parser.add_argument('-redshift',type=float,default=0.5)

   args = parser.parse_args()
   


class template_iter():
    def __init__(self,redshift=0.5,fid_cosmo={},true_cosmo={},temp_par={}):
        #setup the fiducial and true cosmology
        self.redshift=redshift
        self.cosmo={'fid':fid_cosmo,
                    'true':true_cosmo}

        default_dic={'w0':-1}
        for cc,ckey in enumerate(self.cosmo.keys()):
            if(self.cosmo[ckey]=={}):
                continue
            for tt,tkey in enumerate(default_dic.keys()):
                if(tkey not in self.cosmo[ckey].keys()):
                    self.cosmo[ckey][tkey]=default_dic[tkey]

        #setup fiducial
        self.setup_fid(fid_dic=fid_cosmo)

        #setup parameter history
        self.history={}
        for tkey in self.cosmo['fid'].keys():
            self.history[tkey]=[self.cosmo['fid'][tkey]]

        #obtain template parameters
        self.obtain_template_parameter(temp_par=temp_par)

        return

    def setup_fid(self,fid_dic={}):
        '''copy any missing key from true_cosmo otherwise fid_dic is used'''

        for tt,tkey in enumerate(fid_dic.keys()):
            if(tkey in fid_dic.keys()):
                self.cosmo['fid'][tkey]=fid_dic[tkey]

        if(self.cosmo['true']!={}):
            for tt,tkey in enumerate(self.cosmo['true'].keys()):
                if(tkey not in self.cosmo['fid'].keys()):
                    self.cosmo['fid'][tkey]=self.cosmo['true'][tkey]


        return

    def obtain_template_parameter(self,temp_par={}):
        #obtain DA, H and fsigma8 for true and fid


        #obtain alpha_par and alpha_perp
        if(self.cosmo['true']!={}):
            self.prop={'true':{},'fid':{}}
        else:
            self.prop={'fid':{}}

        #angular diameter distance in Mpc/h
        for tkey in self.prop.keys():
            #cosmo=FlatLambdaCDM(H0=self.cosmo[tkey]['H0'], Om0=self.cosmo[tkey]['Om0'])
            #self.prop[tkey]['DAz']=cosmo.angular_diameter_distance(self.redshift).value*cosmo.h
            #self.prop[tkey]['Hz']=cosmo.H(self.redshift).value
            H0=self.cosmo[tkey]['H0'];omM=self.cosmo[tkey]['Om0']
            omL=1.0-omM
            w0=self.cosmo[tkey]['w0']

            self.prop[tkey]['rs']  =mcosm.r_s(omM,self.cosmo[tkey]['Obh2']/np.power(H0/100,2),H0)
            self.prop[tkey]['DAz'] = mcosm.D_A(self.redshift,H0, omM,omL,w0=w0)
            self.prop[tkey]['Hz']  = mcosm.H_z(self.redshift,H0, omM,omL,w0=w0)
            self.prop[tkey]['Omz'] =mcosm.omMz(self.redshift,H0, omM,omL,w0=w0)
            self.prop[tkey]['f']=np.power(self.prop[tkey]['Omz'],0.55)
            self.prop[tkey]['s8z']=self.cosmo[tkey]['sigma8']*mcosm.sigma8z(self.redshift,H0, omM,omL,w0=w0)


        if(temp_par!={}):
            self.temp_par=temp_par
            return
        else:
            self.temp_par={'apar':(self.prop['fid']['Hz']*self.prop['fid']['rs'])/(self.prop['true']['Hz']*self.prop['true']['rs']),
                       'aper':(self.prop['true']['DAz']*self.prop['fid']['rs'])/(self.prop['fid']['DAz']*self.prop['true']['rs']),
                       'fs8':self.prop['true']['f']*self.prop['true']['s8z']
                    }



        return

    def get_proposal(self):
        '''estimate the proposed Omega_m, H0 and s8'''

        DAz_over_rs=self.temp_par['aper']*self.prop['fid']['DAz']/self.prop['fid']['rs']
        Hz_rs= (self.prop['fid']['Hz']*self.prop['fid']['rs'])/self.temp_par['apar']

        self.min_par={}
        #get the omega_m and H0
        self.minimum_om_h(DAz_over_rs,Hz_rs)

        #then get the growth at
        omz=mcosm.omMz(self.redshift,self.min_par['H0'],self.min_par['Om0'],1-self.min_par['Om0'])
        growth_rate=np.power(omz,0.55)
        s8_z0_factor=mcosm.sigma8z(self.redshift,self.min_par['H0'],self.min_par['Om0'],1-self.min_par['Om0'])
        self.min_par['sigma8']=self.temp_par['fs8']/(growth_rate*s8_z0_factor)


        for tkey in self.history.keys():
            if(tkey in self.min_par.keys()):
                self.history[tkey].append(self.min_par[tkey])
            else:
                self.history[tkey].append(self.cosmo['fid'][tkey])


        return

    def build_steps(self,nstep=5):
        '''builds the steps to take'''

        for ii in range(0,nstep):
            #get the next step
            self.get_proposal()

            #setup the new fiducial
            self.setup_fid(fid_dic=self.min_par)

            #obtain template parameters
            self.obtain_template_parameter()

        return

    def minimum_om_h(self,DAz_over_rs,Hz_rs):
        '''minimize the functions'''

        def chi2_DAz_Hz_rs(param):
            Om0=param[0]
            H0=param[1]
            t_DAz=mcosm.D_A(self.redshift,H0,Om0,1-Om0)
            t_Hz=mcosm.H_z(self.redshift,H0,Om0,1-Om0)
            t_rs=mcosm.r_s(Om0,self.cosmo['fid']['Obh2']/np.power(H0/100,2),H0)


            t_DAz_over_rs= t_DAz/t_rs
            t_Hz_rs= t_Hz*t_rs

            chi2= np.sqrt(np.power(t_DAz_over_rs-DAz_over_rs,2)+np.power(t_Hz_rs-Hz_rs,2))

            return chi2

        x0=np.array([self.cosmo['fid']['Om0'],self.cosmo['fid']['H0']])
        res = minimize(chi2_DAz_Hz_rs, x0, method='Nelder-Mead', tol=1e-6)

        self.min_par['res']=res
        self.min_par['Om0']=res.x[0]
        self.min_par['H0']=res.x[1]


        return

def split_parameters(tline):
    '''split the line in parameters'''
    tspl=tline.split()
    
    par_dic={}
    for tt in tspl:
        tp=tt.split('=')
        par_dic[tp[0]]=np.float(tp[1])
        
    return par_dic

def check_par_dic(par_dic,lineno):
    'This checks if all the inputs are provided in the iter file line'
    req_par=['Omega_m','H0','sigma8','apar','aper','fs8']
    
    stat=False
    for tt,tkey in enumerate(req_par):
        if(tkey not in par_dic.keys()):
            print('missing parameter in line %d : %s'%(lineno,tkey))
            stat=True
            
    if(stat==True):
        print('*** Make sure all missing parameters are provided : exiting')
        sys.exit()
        
    return
        
    
def process_file(iter_file,redshift=0.5,w0=-1,Obh2=0.0226):
    '''read the iter file and process to get the next step'''
    
    tlines=open(iter_file).readlines()
    
    par_dic=split_parameters(tlines[-1])
    
    check_par_dic(par_dic,len(tlines))
    
    fid_cosmo={'Om0':par_dic['Omega_m'],'H0':par_dic['H0'],'sigma8':par_dic['sigma8'],
              'w0':w0, 'Obh2':Obh2}
    temp_par={'apar':par_dic['apar'],'aper':par_dic['aper'],'fs8':par_dic['fs8']}
    
    #initialte the titer object
    titer=template_iter(redshift=redshift,fid_cosmo=fid_cosmo,true_cosmo={},temp_par=temp_par)
    titer.get_proposal()
    
    #create line for new parameter
    new_line='Omega_m=%4.2f H0=%4.2f sigma8=%4.2f'%(titer.min_par['Om0'],
                                            titer.min_par['H0'],titer.min_par['sigma8'])
    
    #update the file
    with open(iter_file,'a') as fout:
        fout.write(new_line)
        
    
    #print 'a message'
    msg='last line: %s\n'%tlines[-1]
    msg=msg+' new proposal: %s'%new_line
    
    print('****************\n'+msg)
    
process_file(args.iter_file,args.redshift,Obh2=args.Obh2)


