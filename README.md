# To get the new proposal for RSD template fit

Steps to run:

You should have your first fiducial cosmology and best-fit template result

Create a file with all the fiducial cosmology (Omega_m, H0, sigma8) and template parameter (apar, aper, fs8) as given in the test_iter.txt file for example.

Now run the code to get the new proposal

python RSD_template_iteration.py -iter_file < file-name > -redshift <> -Obh2 <>

You should provide the iter_file you just generated, fiducial redshift and Omega_bh^2 assumed in the analysis.

This will add a new line to your file giving the new fiducial cosmology. It is also printed.

Now you can go back and re-run your analysis to find the best fit template parameter.

Just add this best fit template parameters to the iter_file and re-run to get new proposal.

![LCDM](LCDM.png "LCDM")

![wcdm](wcdm-0.95.png "wcdm")

![Ombh2](Ombh2-5sigma.png "Ombh2")

